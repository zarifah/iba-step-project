$(document).ready(function() {
    $(".navigation .nav-item").click(function() {
        // $(this).addClass("active").siblings().removeClass("active");
        $("active").slideToggle();
    });

    $(".tabs .tab-contents li").click(function() {
        $(this).addClass("active").siblings().removeClass("active").closest(".tabs").find(".tab-content").removeClass("active").eq($(this).index()).addClass("active");
    });
    $(".photos .captions li").click(function() {
        $(this).addClass("active").siblings().removeClass("active").closest(".photos");

        let category = $(this).attr('data-category');

        $(this).closest(".photos").find(".content img").each(function(i, item) {
            if ($(item).attr('data-category') == category || category == "all") {
                $(item).slice(0, 1).show();
            } else {
                $(item).hide();
            }
        })

    });

    $(".our-work-section .btn.load").click(function() {
        loadWorks();
        if (countWorks >= photosLibrary.length) {
            $(this).hide();
        }
    });
    getRandomArray(photosLibrary);
    loadWorks();
    let slide = new Slider("#testimonial");

});

let photosLibrary = [
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design1.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design2.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design3.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design4.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design5.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design6.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design7.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design8.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design9.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design10.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design11.jpg" },
    { category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design12.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page1.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page2.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page3.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page4.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page5.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page6.jpg" },
    { category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page7.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design1.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design2.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design3.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design4.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design5.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design6.jpg" },
    { category: "web-design", caption: "Web Design", file: "/web design/web-design7.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress1.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress2.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress3.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress4.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress5.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress6.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress7.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress8.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress9.jpg" },
    { category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress10.jpg" }
];


let countWorks = 0;
let maxLoading = 12;

function loadWorks() {
    if (countWorks < photosLibrary.length) {
        let tmp = countWorks < photosLibrary.length - maxLoading ? countWorks + maxLoading : photosLibrary.length;
        for (let i = countWorks; i < tmp; i++) {

            let newItem = $("<div>").addClass("hov");
            $(".our-works .content").append(newItem);


            let item = $("<img/>")
                .attr("src", "img" + photosLibrary[i].file)
                .attr("data-category", photosLibrary[i].category)
                .attr("drag", false);
            $(newItem).append(item);

            let itemInfo = $("<div>").addClass("info")
                .append("<div class='info-btn-hov'>" +
                    "<div class='info-btn link'>" +
                    "<i class='fa fa-link' '></i>" +
                    "</div>" +
                    "<div class='info-btn search'>" +
                    "<i class='fa fa-search' '></i>" +
                    "</div>" +
                    "</div>")
                .append("<p class='title'> creative design </p>")
                .append("<p class='caption'>" + photosLibrary[i].caption + "</p>");
            $(newItem).append(itemInfo);

            countWorks++;
        }
    }
}


function getRandomArray(arr) {
    arr.forEach(function(item) {
        item.sortOrder = Math.random();
    });
    arr = arr.sort(function(order1, order2) {
        return order1.sortOrder - order2.sortOrder;
    });
}